const { mongoose } = require('../config/mongoose')
const { TRANSACTION_OPERATIONS, TRANSACTION_STATUS, STATUS_INITIALIZED } = require('../config/constants')
require('mongoose-double')(mongoose)

const transactionSchema = new mongoose.Schema({
    marchandId: {
        type: String,
        required: true,
    },
    amount: {
        type: mongoose.Schema.Types.Double,
        required: true,
    },
    devise: {
        type: String,
        required: true
    },
    reference: {
        type: mongoose.Schema.Types.ObjectId,
    },
    operation: {
        type: String,
        enum: TRANSACTION_OPERATIONS,
    },
    paymentDetails: {
        type: Map
    },
    status: {
        type: String,
        enum: TRANSACTION_STATUS,
        required: true
    }
}, {
    timestamps: true
})

transactionSchema.plugin(require('mongoose-lifecycle'))

const Transaction = mongoose.model('Transaction', transactionSchema)

Transaction.on('afterInsert', function (tr) {
    if (tr.status === STATUS_INITIALIZED)
        tr.update({reference: tr._id})
})


module.exports = { Transaction }