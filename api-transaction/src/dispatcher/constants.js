module.exports = {
    TRANSACTION_INITIATED: 'transaction-initiated',
    TRANSACTION_VALIDATED: 'transaction-validated',
    TRANSACTION_PAYED: 'transaction-payed',
}
