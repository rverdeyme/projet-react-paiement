const jwt = require('jsonwebtoken')
const pug = require('pug')
const fetch = require('node-fetch')
const { getToken } = require('../token')
const { Transaction } = require('../models')
const { STATUS_VALIDATED, OPERATION_PAYMENT, STATUS_INITIALIZED, STATUS_CANCELED, STATUS_PAYED } = require('../config')
const { eventEmitter } = require('../dispatcher')
const { TRANSACTION_VALIDATED, TRANSACTION_PAYED, TRANSACTION_INITIATED } = require('../dispatcher/constants')

module.exports = {
    /**
     * Creates a transaction in the database
     * Creates the correct tokens (for the payment's page url, and for the cancel/validate url)
     * Since this service is designed to be stateless, all information pass through the token
     */
    createTransaction: async (req, res) => {
        try {
            // TODO -> Add other elements
            // TODO -> Add verification on elements
            const {
                amount
            } = req.body

            delete req.body.amount

            console.log(req.body)

            const { confirmUrl, cancelUrl, devise } = req.options

            if (!confirmUrl || !cancelUrl) throw new Error('You must provide confirm and cancel urls')
            if (!amount) throw new Error('You must provide the amount')

            const paymentDetails = new Map(Object.entries(req.body))
            /**
             * Creating the transaction on the database
             * Since this is the initial transaction, we pass it the correct status and don't set a reference
             * (All other operation on this transaction will have this one's id as a reference)
             */
            const transaction = await Transaction.create({
                paymentDetails, amount, devise,
                marchandId: req.marchandId,
                status: STATUS_INITIALIZED
            })
            const { _id: transactionId } = transaction

            eventEmitter.emit(TRANSACTION_INITIATED, { marchandId: req.marchandId, amount, devise })

            const transactionToken = getToken({
                transactionId, confirmUrl, cancelUrl
            })

            const token = getToken({
                devise, amount, transactionId, transactionToken,
            })

            res.send({
                success: true,
                paymentUrl: `https://localhost/api/transaction/pay/${token}`
            })
        } catch (error) {
            console.log(error)
            res.status(400).send({
                success: false,
                message: error.message || 'Something went wrong with the initialization'
            })
        }
    },

    /**
     * Creates a new transaction on the database
     * Verifies if everything is correct (no previous cancel/validation of the transaction)
     * Sends a request to the psp
     * Redirects to the url configured by the marchand
     */
    validateTransaction: async (req, res) => {
        try {
            // TODO -> add more elements
            // TODO -> verification
            // TODO -> add card details, fraud, ... here
            const { transactions, paymentDetails: { confirmUrl } } = req

            const { nameOnCard, cardNumber } = req.body

            // Check if there is already a validation on this transaction
            if (transactions.some(({ status }) => status === STATUS_VALIDATED)) throw new Error('There already is a validated transaction')

            // Check if there is already a cancelation on this transaction
            if (transactions.some(({ status }) => status === STATUS_CANCELED)) throw new Error('The transaction has been canceled')

            // Select the initial transaction
            const { amount, devise, marchandId, _id: initialTransactionId } = transactions.find(({ status }) => status === STATUS_INITIALIZED)

            // Creates the new transaction line with the correct informations
            await Transaction.create({
                amount, devise, marchandId,
                reference: initialTransactionId,
                status: STATUS_VALIDATED,
                operation: OPERATION_PAYMENT,
            })

            eventEmitter.emit(TRANSACTION_VALIDATED, { marchandId, amount, devise })

            res.redirect(confirmUrl)

            await fetch(`${process.env.BASE_URL}psp/`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    amount, devise, cardDetails: {
                        nameOnCard, cardNumber
                    }
                })
            })

            // Creates the new transaction line with the correct informations
            await Transaction.create({
                amount, devise, marchandId,
                reference: initialTransactionId,
                status: STATUS_PAYED,
                operation: OPERATION_PAYMENT,
            })

            eventEmitter.emit(TRANSACTION_PAYED, { marchandId, amount, devise })

        } catch (error) {
            console.log(error)
            res.status(400).send({
                success: false,
                message: error.message || 'Something went wrong with the transaction.'
            })
        }
    },

    /**
     * Creates a new transaction on the database
     * Sets the status to cancel
     * Redirects
     */
    cancelTransaction: async (req, res) => {
        try {
            const { transactionId, cancelUrl } = req.paymentDetails
            const { marchandId, amount, devise } = await Transaction.findById(transactionId)

            await Transaction.create({
                reference: transactionId,
                status: STATUS_CANCELED,
                marchandId, amount, devise
            })

            res.redirect(cancelUrl)
        } catch (error) {
            console.log(error)
            res.status(400).send({
                success: false,
                message: error.message || 'Something went wrong with the transaction.'
            })
        }
    },

    /**
     * Middleware
     * Search the database for the transactions history (by reference && _id)
     * Pass the transactions found in the req object
     */
    getTransactionInfo: async (req, res, next) => {
        const { transactionId } = req.paymentDetails

        try {
            const transactions = await Transaction.find({
                // Either reference or by _id
                $or: [{ reference: transactionId }, { _id: transactionId }]
            }).lean()

            // There must be an initial transaction
            if (transactions.length === 0) throw new Error('No initial payment found')

            req.transactions = transactions
            next()
        } catch (error) {
            console.log(error)
            res.status(400).send({
                success: false,
                message: error.message || 'Something went wrong with transaction details. Please try again later',
            })
        }
    },

    /**
     * Use pug to generate the payment template
     */
    generatePaymentTemplate: async (req, res) => {
        // Check if there is already a validation on this transaction
        if (req.transactions
            .some(
                ({ status }) => status === STATUS_VALIDATED || status === STATUS_CANCELED
            ))
            res.status(403).send({
                success: false,
                message: 'The transaction hase already been validated or canceled'
            })
        else
            res.send(pug.compileFile('src/transaction/template/paymentPage.pug')({
                ...req.paymentDetails
            }))
    }
}
