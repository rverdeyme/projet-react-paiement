const { verifyJWT, getTokenPaymentInfo, getMarchandOptions } = require('../token/tokenHandle')
const { createTransaction, generatePaymentTemplate, validateTransaction, getTransactionInfo, cancelTransaction } = require('./controller')

module.exports = app => {

    const router = require('express').Router()

    /**
     * Initialize a transaction
     */
    router.post('/initialize', verifyJWT, getMarchandOptions, createTransaction)

    /**
     * Providing the jwt, returns an HTML page to provide payment options
     */
    router.get('/pay/:token', getTokenPaymentInfo, getTransactionInfo, generatePaymentTemplate)

    /**
     * Route for validating the payment
     */
    router.post('/validate/:token', getTokenPaymentInfo, getTransactionInfo, validateTransaction)

    /**
     * Route for canceling the payment
     */
    router.get('/cancel/:token', getTokenPaymentInfo, cancelTransaction)

    app.use('/', router)
}
