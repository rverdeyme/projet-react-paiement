/**
 * This file is composed of constants for the transaction model
 */

const constants = {
    OPERATION_PAYMENT: 'payment',
    OPERATION_REFUND: 'refund',
    STATUS_INITIALIZED: 'initialized',
    STATUS_VALIDATED: 'valdiated',
    STATUS_PAYED: 'payed',
    STATUS_CANCELED: 'canceled',
    STATUS_ERROR: 'error'
}

const TRANSACTION_OPERATIONS = [constants.OPERATION_INITIAL, constants.OPERATION_PAYMENT, constants.OPERATION_REFUND]
const TRANSACTION_STATUS = [constants.STATUS_INITIALIZED, constants.STATUS_PAYED, constants.STATUS_VALIDATED, constants.STATUS_ERROR, constants.STATUS_CANCELED]

module.exports = {
    ...constants,
    TRANSACTION_OPERATIONS,
    TRANSACTION_STATUS
}
