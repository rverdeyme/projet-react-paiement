const { Transaction } = require('../models')

module.exports = {
    /**
     * Route to fetch a marchand's transaction
     * Little bit of formatting
     *  - transactions are sorted by reference
     *  - we have an initialTransaction object
     */
    getAllMarchandTransaction: async (req, res) => {
        const { marchandId } = req

        try {
            const transactions = await Transaction.aggregate([
                {$match: { marchandId: String(marchandId) }},
                {$group: {
                    _id: '$reference',
                    transactions: {$push: '$$ROOT'},
                }}
            ])

            res.send({data: transactions})
        } catch (error) {
            console.log(error)
            res.status(400).send({
                success: false,
                message: error.message || 'Something went wrong with the data'
            })
        }
    }
}
