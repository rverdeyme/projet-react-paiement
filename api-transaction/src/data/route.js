const { verifyJWT } = require('../token/tokenHandle')
const { getAllMarchandTransaction } = require('./controller')

module.exports = app => {

    const router = require('express').Router()

    router.get('/', verifyJWT, getAllMarchandTransaction)

    router.get('/', )

    app.use('/data', router)

}
