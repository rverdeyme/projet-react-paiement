const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const helmet = require('helmet')
const spdy = require('spdy')
const cors = require('cors');

const { clients, clientRouter } = require('./src/clients')
const serverRouter = require('./src/servers')

const app = express()

app.use(helmet())
// parse requests of content-type - application/json
app.use(bodyParser.json())

app.use(cors())

app.use(morgan('dev'))

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

app.use(clientRouter)
app.use(serverRouter)
app.get('/status', (req, res) => res.json({ clients: clients.length }))

const PORT = process.env.API_DISPATCHER_PORT || 3000
const server = spdy.createServer(require('./ssl'), app)

server.listen(PORT, (error) => {
    if (error) {
        console.error(error)
    } else {
        console.log(`Listening on port: ${PORT}`)
    }
})

module.exports = app
