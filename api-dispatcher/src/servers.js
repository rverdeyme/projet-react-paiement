const eventsEmitter = require('./events')
const { verifyApiJWT } = require('./token')
const { TRANSACTION } = require('./constants')

const transactionHandler = async (req, res) => {
    const action = req.body

    eventsEmitter.emit(TRANSACTION, action)

    res.send({ success: true })
}

const serverRouter = require('express').Router()

serverRouter.post('/transaction', verifyApiJWT, transactionHandler)

module.exports = serverRouter

