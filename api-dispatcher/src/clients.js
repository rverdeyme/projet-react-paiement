const eventEmitter = require('./events')
const constants = require('./constants')
const { verifyJWT } = require('./token')

let clients = []

eventEmitter.on(constants.NEW_CLIENT, ({ marchandId, res }) => {

    const client = {
        id: Date.now(),
        marchandId,
        res
    }
    clients.push(client)
})

eventEmitter.on(constants.CLIENT_DISCONNECTED, id =>
    clients = clients.filter(({ id: _id }) => id === _id)
)


eventEmitter.on(constants.TRANSACTION, ({ type, payload }) => {
    try {
        const connectedClients = clients.filter(({ marchandId }) => String(payload.marchandId) === String(marchandId))

        if (connectedClients.length === 0) {
            console.log('No client connected', {
                type, payload,
            })
            console.log({ clients })
            return
        }

        const data = {
            type,
            ...payload
        }


        for (const { res } of connectedClients) {
            res.write(`data: ${JSON.stringify(data)}`)
            res.write('\n\n')
        }
    } catch (error) {
        console.error(error)
    }
})

function clientHandler(req, res) {
    const headers = {
        'Content-Type': 'text/event-stream',
        'Connection': 'keep-alive',
        'Cache-Control': 'no-cache'
    }

    res.writeHead(200, headers)
    res.write('connected\n\n')

    const { marchandId } = req.token

    console.log(`Client connected with marchand id : ${marchandId}`)
    const id = Date.now()
    eventEmitter.emit(constants.NEW_CLIENT, { id, res, marchandId: marchandId })

    req.on('close', () => {
        console.log(`Client Disconnected with marchand id : ${marchandId}`)
        eventEmitter.emit(constants.CLIENT_DISCONNECTED, id)
    })
}

const clientRouter = require('express').Router()

clientRouter.get('/:token', verifyJWT, clientHandler)


module.exports = {
    clientRouter,
    clients
}


