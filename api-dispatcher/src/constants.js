module.exports = {
    NEW_TRANSACTION: 'transaction-new',
    NEW_CLIENT: 'client-new',
    TRANSACTION_VALIDATED: 'transaction-validated',
    TRANSACTION_PAYED: 'transaction-payed',
    TRANSACTION: 'transaction-event'
}