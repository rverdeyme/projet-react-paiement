const jwt = require('jsonwebtoken')

module.exports = {    /**
     * Middleware
     * Verifies the token passed as a header or as a parameter (for payment routes)
     * Extract the marchandId from the token
     * Adds it to the req object
     */
    verifyJWT: async (req, res, next) => {
        try {
            let token = req.headers['authorization'] || req.params.token

            if (!token) throw new Error('No token provided')

            token = token.replace('Bearer ', '')

            token = jwt.verify(token, process.env.JWT_SECRET || 'prp-esgi')

            if (!token.marchandId) throw new Error('There was an error with the marchand account')

            req.token = token

            next()
        } catch (error) {
            console.log(error)
            return res.status(401).json({
                success: false,
                message: error.message || 'Something went wrong with your token',
            })
        }
    },
    verifyApiJWT: async (req, res, next) => {
        try {
            let token = req.headers['authorization']

            if (!token) throw new Error('No token provided')

            token = token.replace('Bearer ', '')

            jwt.verify(token, process.env.JWT_SECRET || 'prp-esgi')

            next()
        } catch (error) {
            console.log(error)
            return res.status(401).json({
                success: false,
                message: error.message || 'Something went wrong with your token',
            })
        }
    },
}                     