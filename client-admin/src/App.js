import React from 'react';
import { Route, Switch } from 'react-router-dom'
import Dashboard from './dashboard/Dashboard'
import './App.css';
import Marchand from "./marchand/marchand";
import Transaction from "./transaction/transaction"
import Stats from "./stats/stats";
import MarchandDetails from "./marchand/marchandDetails";
import NavigationMenu from "./navigation/navigationMenu";
import { makeStyles } from '@material-ui/core/styles';
import TransactionDetails from "./transaction/transactionDetails";

const useStyle = makeStyles({
  root: {
    display: 'flex',
  },
});

function App() {
  const classes = useStyle();

  return (
    <div className="App">
      <Switch>

        <Route exact path="/">
          <div className={classes.root}>
            <NavigationMenu />
            <Dashboard />
          </div>
        </Route>

        <Route path="/dashboard">
          <div className={classes.root}>
            <NavigationMenu />
            <Dashboard />
          </div>
        </Route>

        <Route path="/transactions/:id">
          <div className={classes.root}>
            <NavigationMenu />
            <TransactionDetails/>
          </div>
        </Route>

        <Route path="/transactions">
          <div className={classes.root}>
            <NavigationMenu />
            <Transaction />
          </div>
        </Route>

        <Route path="/marchands/:id">
          <div className={classes.root}>
            <NavigationMenu />
            <MarchandDetails/>
          </div>
        </Route>

        <Route path="/marchands">
          <div className={classes.root}>
            <NavigationMenu />
            <Marchand />
          </div>
        </Route>

        <Route path="/stats">
          <div className={classes.root}>
            <NavigationMenu />
            <Stats />
          </div>
        </Route>
      </Switch>
    </div>
  );
}

export default App;
