import React, { useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Container from '@material-ui/core/Container';
import Divider from '@material-ui/core/Divider';
import Fab from '@material-ui/core/Fab';
import EditIcon from '@material-ui/icons/Edit';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import BeenhereOutlinedIcon from '@material-ui/icons/BeenhereOutlined';
import { useRouteMatch } from 'react-router-dom';

const useStyle = makeStyles( (theme) => ({
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
    margin: '24px',
    marginTop: '72px'
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
}));

const SocietyInformationsCard = ( { marchand, setMarchand } ) => {


  const validateMarchand = () => {
    // let response = await fetch('url',{
    //   method: 'POST',
    //   headers: { 'Content-Type': 'application/json' },
    //   body: JSON.stringify({validated: true}),
    // })

    fetch('https://localhost/api/marchand/admin/marchands/' + marchand.id, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        ...marchand,
        validated: true
      }
      ),
    }).then( (response) => {
      response.json().then( (data) => {
        setMarchand({
          ...marchand,
          validated: true
        })
      })
    })

  }

  return <Paper>
    <h2>Society informations</h2>

    <Divider />

    <p>Society name: {marchand.societyName}</p>
    <p>Kbis: {marchand.kbis}</p>
    <p>Validated: {marchand.validated ? 'Oui' : 'Non'}</p>

    <Divider />

    {/*<Fab color="primary" aria-label="edit">*/}
    {/*  <EditIcon />*/}
    {/*</Fab>*/}

    <Fab color="secondary" aria-label="delete">
      <HighlightOffIcon />
    </Fab>

    {!marchand.validated && <Fab color="default" aria-label="validate" onClick={validateMarchand}>
      <BeenhereOutlinedIcon/>
    </Fab>
    }

  </Paper>
}

const ContactInformationsCard = ({ contact }) => {
  return <Paper>
    <h2>Contact informations</h2>

    <Divider />

    <p>Contact name: {contact.name}</p>
    <p>Address: {contact.address}</p>
    <p>City: {contact.city}</p>
    <p>Pays: {contact.country}</p>
    <p>E-mail: {contact.email}</p>

    <Divider />

    {/*<Fab color="primary" aria-label="edit">*/}
    {/*  <EditIcon />*/}
    {/*</Fab>*/}

    <Fab color="secondary" aria-label="edit">
      <HighlightOffIcon />
    </Fab>

  </Paper>
}

const OtherInformationsCard = () => {
  return <Paper>
    <h2>Other informations</h2>

    <Divider />
    <p>Panier moyen: valeur panier moyen</p>
    <p>Nombre de transaction total:  666</p>
    <p>Nombre moyen de transaction par jour:  666</p>

  </Paper>
}

export default function MarchandDetails( ) {
  const classes = useStyle();

  let match = useRouteMatch();
  const [marchand, setMarchand] = useState([]);
  const [contact, setContact] = useState([]);

  useEffect(  () => {
    let id = match.params.id;

    fetch('https://localhost/api/marchand/admin/marchands/' + id, {
      method: 'GET',
    }).then( (response) => {
      response.json().then( (data) => {
        setMarchand(data)
      })
    })

    fetch('https://localhost/api/marchand/admin/contact/' + id, {
      method: 'GET',
    }).then( (response) => {
      response.json().then( (data) => {
        setContact(data)
      })
    })

  }, []);

  return (
    <div className={classes.content}>
      <h1>Page détail du marchand</h1>

      <Container maxWidth="lg" className={classes.container}>

        <Grid container spacing={3}>

          <Grid item xs={12} md={12} lg={6}>
            <SocietyInformationsCard marchand={marchand} setMarchand={setMarchand}/>
          </Grid>

          <Grid item xs={12} md={12} lg={6}>
            <ContactInformationsCard contact={contact}/>
          </Grid>

          <Grid item xs={12} md={12} lg={12}>
            <OtherInformationsCard/>
          </Grid>
        </Grid>

      </Container>
    </div>
  )
}
