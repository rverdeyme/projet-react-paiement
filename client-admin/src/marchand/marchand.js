import React, { useState, useEffect } from 'react'
import MarchandTable from "./marchandTable"
import { makeStyles } from '@material-ui/core/styles'

const useRowStyles = makeStyles({
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
    margin: '24px',
    marginTop: '72px'
  },
});

export default function Marchand() {
  const classes = useRowStyles();
  const [marchands, setMarchands] = useState([]);

  useEffect(  () => {

   fetch('https://localhost/api/marchand/admin/marchands', {
      method: 'GET',
    }).then( (response) => {
      response.json().then( (data) => {
        setMarchands(data)
      })
   })
    

  }, []);


  return (
    <div className={classes.content}>
      <h1>Page marchands</h1>
      <MarchandTable marchands={marchands}/>
    </div>
  )
}
