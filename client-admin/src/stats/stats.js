import React from 'react'
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
    margin: '24px',
    marginTop: '72px'
  },
});

export default function Stats() {
  const classes = useStyles();

  return (
    <div className={classes.content}>
      <h1>Page des stats</h1>
    </div>
  )
}
