import React from 'react'
import TransactionTable from "./transactionTable";
import { makeStyles } from '@material-ui/core/styles';

const useRowStyles = makeStyles({
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
    margin: '24px',
    marginTop: '72px'
  },
});

export default function Transaction() {
  const classes = useRowStyles();

  return (
    <div className={classes.content}>
      <h1>Page transactions</h1>
      <TransactionTable/>
    </div>
  )
}
