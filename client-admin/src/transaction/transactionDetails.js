import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Container from '@material-ui/core/Container';
import Divider from '@material-ui/core/Divider';
import Fab from '@material-ui/core/Fab';
import EditIcon from '@material-ui/icons/Edit';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import BeenhereOutlinedIcon from '@material-ui/icons/BeenhereOutlined';

const useStyle = makeStyles( (theme) => ({
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
    margin: '24px',
    marginTop: '72px'
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
}));

const OperationCard = ({ operation }) => {
  return <Paper>
    <h2>Card operation</h2>

    <Divider />

    <p>Montant: {'Montant'}</p>
    <p>Devise: {'Devise '}</p>
    <p>Type d'opération: {'Type d\'opération'}</p>
    <p>Date: {'Date'}</p>

  </Paper>
}

export default function TransactionDetails({ marchand }) {
  const classes = useStyle();

  return (
    <div className={classes.content}>
      <h1>Page détail d'une transaction</h1>

      <Container maxWidth="lg" className={classes.container}>

        <Grid container spacing={3}>

          <Grid item xs={12} md={12} lg={4}>
            <OperationCard/>
          </Grid>

          <Grid item xs={12} md={12} lg={4}>
            <OperationCard/>
          </Grid>

          <Grid item xs={12} md={12} lg={4}>
            <OperationCard/>
          </Grid>

          <Grid item xs={12} md={12} lg={4}>
            <OperationCard/>
          </Grid>

          <Grid item xs={12} md={12} lg={4}>
            <OperationCard/>
          </Grid>

          <Grid item xs={12} md={12} lg={4}>
            <OperationCard/>
          </Grid>

          <Grid item xs={12} md={12} lg={4}>
            <OperationCard/>
          </Grid>

          <Grid item xs={12} md={12} lg={4}>
            <OperationCard/>
          </Grid>

          <Grid item xs={12} md={12} lg={4}>
            <OperationCard/>
          </Grid>


        </Grid>

      </Container>
    </div>
  )
}
