import React, { useState } from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableFooter from '@material-ui/core/TableFooter';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import TablePagination from '@material-ui/core/TablePagination';
import LastPageIcon from '@material-ui/icons/LastPage';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import SearchIcon from '@material-ui/icons/Search';
import Divider from '@material-ui/core/Divider';
import InputBase from '@material-ui/core/InputBase';
import { Link, useRouteMatch } from 'react-router-dom';

const useStyles = makeStyles( (theme) => ({
  paginationActions: {
    flexShrink: 0,
    marginLeft: theme.spacing(2.5),
  },
  root: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: 400,
    float: 'right',
    marginBottom: 24
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    height: 28,
    margin: 4,
  },
  tableCell: {
    fontSize: "15px"
  }
}));

function createData(marchand, amount, devise, status, date) {
  return {
    marchand,
    amount,
    devise,
    status,
    date,
    history: [
      { date: '2020-01-05', customerId: '11091700', amount: 3 },
      { date: '2020-01-02', customerId: 'Anonymous', amount: 1 },
    ],
  };
}

function TablePaginationActions(props) {
  const classes = useStyles();
  const theme = useTheme();
  const { count, page, rowsPerPage, onChangePage } = props;

  const handleFirstPageButtonClick = (event) => {
    onChangePage(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onChangePage(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onChangePage(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
      <div className={classes.paginationActions}>
        <IconButton
            onClick={handleFirstPageButtonClick}
            disabled={page === 0}
            aria-label="first page"
        >
          {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
        </IconButton>
        <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
          {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
        </IconButton>
        <IconButton
            onClick={handleNextButtonClick}
            disabled={page >= Math.ceil(count / rowsPerPage) - 1}
            aria-label="next page"
        >
          {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
        </IconButton>
        <IconButton
            onClick={handleLastPageButtonClick}
            disabled={page >= Math.ceil(count / rowsPerPage) - 1}
            aria-label="last page"
        >
          {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
        </IconButton>
      </div>
  );
}

function Row(props) {
  let match = useRouteMatch();
  const { row } = props;
  const [open, setOpen] = useState(false);

  return (
      <React.Fragment>

        <TableRow>
          <TableCell>
            <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
              {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
            </IconButton>
          </TableCell>
          <TableCell component="th" scope="row">
            <Link to={`${match.url}/${row.marchand}`}>{row.marchand} </Link>
          </TableCell>
          <TableCell align="right">{row.amount}</TableCell>
          <TableCell align="right">{row.devise}</TableCell>
          <TableCell align="right">{row.status}</TableCell>
          <TableCell align="right">{row.date}</TableCell>
        </TableRow>


        <TableRow>
          <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
            <Collapse in={open} timeout="auto" unmountOnExit>
              <Box margin={1}>
                <Typography variant="h6" gutterBottom component="div">
                  History
                </Typography>
                <Table size="small" aria-label="purchases">
                  <TableHead>
                    <TableRow>
                      <TableCell>Date</TableCell>
                      <TableCell>Customer</TableCell>
                      <TableCell align="right">Amount</TableCell>
                      <TableCell align="right">Total price ($)</TableCell>
                      <TableCell align="right">Total price ($)</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {row.history.map((historyRow) => (
                        <TableRow key={historyRow.date}>
                          <TableCell component="th" scope="row">
                            {historyRow.date}
                          </TableCell>
                          <TableCell>{historyRow.customerId}</TableCell>
                          <TableCell align="right">{historyRow.amount}</TableCell>
                          <TableCell align="right">
                            {Math.round(historyRow.amount * row.price * 100) / 100}
                          </TableCell>
                        </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </Box>
            </Collapse>
          </TableCell>
        </TableRow>

      </React.Fragment>
  );
}

function SearchBar({ searchFunction }) {
  const classes = useStyles();

  const handleSearch = (e) => {
    searchFunction(e.target.value)
  }

  return (
      <Paper component="form" className={classes.root}>
        <InputBase
            className={classes.input}
            placeholder="Search for transactions"
            inputProps={{ 'aria-label': 'Search for transactions' }}
            onChange={handleSearch}
        />
        <Divider className={classes.divider} orientation="vertical" />
        <IconButton className={classes.iconButton} aria-label="search">
          <SearchIcon />
        </IconButton>
      </Paper>
  );
}


const rows = [
  createData('Fnac', 159, 'euros (€)', 'validated', '2020-03-12'),
  createData('Carrefour', 237, 'euros (€)', 'payed', '2020-03-14'),
  createData('Apple', 262, 'dollars ($)', 'canceled', '2020-06-20'),
  createData('Tati', 305, 'dollars ($)', 'error', '2020-02-19'),
  createData('KFC', 356, 'euros (€)', 'validated', '2020-07-10'),
  createData(Date.now(), 356, 'euros (€)', 'validated', '2020-07-10')
];

export default function TransactionTable() {
  const classes = useStyles();

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const [transactions, setTransactions] = React.useState(rows);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const searchFunction = (searchValue) => {
    let filteredTransactions = rows.filter( (row) => {
      let marchand = row.marchand.toString().toLowerCase()
      return marchand.includes(searchValue)
    })
    setTransactions(filteredTransactions);
  }

  return (
      <>
        <SearchBar searchFunction={searchFunction}/>
        <TableContainer component={Paper}>
          <Table aria-label="collapsible table">
            <TableHead>
              <TableRow>
                <TableCell className={classes.tableCell}/>
                <TableCell className={classes.tableCell}>Marchand</TableCell>
                <TableCell className={classes.tableCell} align="right">Montant</TableCell>
                <TableCell className={classes.tableCell} align="right">Devise</TableCell>
                <TableCell className={classes.tableCell} align="right">Status</TableCell>
                <TableCell className={classes.tableCell} align="right">Date</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {(rowsPerPage > 0
                      ? transactions.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                      : transactions
              ).map((transaction) => (
                  <Row key={transaction.marchand} row={transaction} />
              ))}
            </TableBody>

            <TableFooter>
              <TableRow>
                <TablePagination
                    rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
                    colSpan={3}
                    count={transactions.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    SelectProps={{
                      inputProps: { 'aria-label': 'rows per page' },
                      native: true,
                    }}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                    ActionsComponent={TablePaginationActions}
                />
              </TableRow>
            </TableFooter>
          </Table>
        </TableContainer>
      </>
  );
}
