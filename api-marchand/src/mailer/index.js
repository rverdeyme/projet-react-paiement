const mailer = require('nodemailer')


module.exports = {

    sendCreationMail: (contact) => {
        try {

            var transporter = mailer.createTransport({
                service: 'gmail',
                auth: {
                    user: 'prp.esgi@gmail.com',
                    pass: 'Prpesgi2020' 
                }
            })

            var mailOptions = {
                from: 'prp.esgi@gmail.com',
                to: contact.email,
                subject: 'Creation Account',
                text: `Hello ${contact.name},\n
                Your account has been created but must be valited by Admin.\n
                Please wait until the validation and you'll can accees to the app.\n
                \n
                PRP ESGI
                `
            }


            console.log(transporter)

            transporter.sendMail(mailOptions, function(error, info){
                if (error) {
                    console.log(error)
                } else {
                    console.log('Email sent: ' + info.response)
                }
            })


        } catch (error) {
            console.log(error)

        }

    }

}