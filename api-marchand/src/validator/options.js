const { Validator } = require('jsonschema')
const v = new Validator()

module.exports = {
    verifyOptions: (options) => {

        if (!options) {
            throw new Error('Options not found')
        }

        let optionsSchema = {
            'type': 'object',
            'properties': {
                'confirmUrl': {
                    'type': 'string',
                    'format': 'url',
                    'minLength': 10,
                },
                'cancelUrl': {
                    'type': 'string',
                    'format': 'url',
                    'minLength': 10,
                },
                'devise': {
                    'type': 'string',
                    'minLength': 1,
                },
                'marchandId': {
                    'type': 'integer',
                }
            },
            'required': ['marchandId']
        }

        let result = v.validate(options, optionsSchema)

        // if errors
        if (result.errors.length) throw new Error(result.errors)

        return result
    }

}