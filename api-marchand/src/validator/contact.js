const { Validator } = require('jsonschema')

module.exports = {
    verifyContact: (contact) => {

        if (!contact) {
            throw new Error('Contact not found')
        }

        let validator = new Validator()
        let contactSchema = {
            'type': 'object',
            'properties': {
                'email': {
                    'type' : 'string',
                    'format': 'email',
                    'errorMessage': 'Email id is missing or uncorrect.'
                },
                'marchandId': {
                    'type': 'integer',
                    'errorMessage': 'Marchand id is missing or uncorrect.'
                }
            },
            'required': []
        }

        let result = validator.validate(contact, contactSchema)

        // if errors
        if (Array.isArray(result.errors) && result.errors.length) {
            let failedInputs = ''

            result.errors.forEach( (error) => {
                failedInputs += error.schema.errorMessage || error.message + ' '
            })

            throw new Error(failedInputs)
        }
    },
    verifyContactOnCreateWithMarchand: (contact) => {

        if (!contact) {
            throw new Error('Contact not found')
        }

        let validator = new Validator()
        let contactSchema = {
            'type': 'object',
            'properties': {
                'email': {
                    'type' : 'string',
                    'format': 'email',
                    'errorMessage': 'Email id is missing or uncorrect.'
                }
            },
            'required': ['email']
        }

        let result = validator.validate(contact, contactSchema)

        // if errors
        if (Array.isArray(result.errors) && result.errors.length) {
            let failedInputs = ''

            result.errors.forEach( (error) => {
                failedInputs += error.schema.errorMessage || error.message + ' '
            })

            throw new Error(failedInputs)
        }
    }

}