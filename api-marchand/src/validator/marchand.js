const { Validator } = require('jsonschema')

module.exports = {
    verifyMarchand: (marchand) => {

        if (!marchand) {
            throw new Error('Marchand not found')
        }

        let validator = new Validator()
        let marchandSchema = {
            'type': 'object',
            'properties': {
                'societyName': {
                    'type': 'string',
                    'minLength': 1,
                    'errorMessage': 'Society name id is missing or incorrect.'
                },
                'password': {
                    'type': 'string',
                    'minLength': 1,
                    'errorMessage': 'Password id is missing or incorrect.'
                }
            },
            'required': ['societyName', 'password']
        }

        let result = validator.validate(marchand, marchandSchema)
        // if errors
        if (Array.isArray(result.errors) && result.errors.length) {
            let failedInputs = ''

            result.errors.forEach((error) => {
                failedInputs += error.schema.errorMessage || error.message + ' '
            })

            throw new Error(failedInputs)
        }
    }

}