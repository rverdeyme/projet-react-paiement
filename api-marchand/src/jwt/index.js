const jwt = require('jsonwebtoken')
const { Marchand } = require('../model')

module.exports = {
    generateToken: function (marchand) {
        //1. Dont use password and other sensitive fields
        //2. Use fields that are useful in other parts of the     
        //app/collections/models
        return jwt.sign({ marchandId: marchand.id }, process.env.JWT_SECRET || 'pgp-esgi')
    },

    verifyJWT: async (req, res, next) => {
        try {
            let token = req.headers['authorization']

            if (!token) throw new Error('No token provided')

            token = token.replace('Bearer ', '')

            const { marchandId } = jwt.verify(token, process.env.JWT_SECRET)

            if (!marchandId) throw new Error('Not a valid token')

            req.marchand = await Marchand.findByPk(marchandId)
            if (!req.marchand) throw new Error('Cannot find marchand')
            next()
        } catch (error) {
            return res.status(404).send({
                success: false,
                message: error.message || 'Something went wrong',
            })
        }

    },



    verifyJWTAdmin: async (req, res, next) => {
        let token = req.headers['authorization']

        if (!token) { res.status(403).send({ message: 'No token provided' }) }

        token = token.replace('Bearer ', '')

        try {

            const { admin } = jwt.verify(token, process.env.JWT_SECRET_ADMIN || 'prp-esgi-admin')
            if (!admin) throw new Error('Not a valid token')

            req.role = {admin: true}
            if (!req.role) throw new Error('User not admin')

            // console.log(req.body.marchandId)

        } catch (error) {
            return res.status(404).send({
                success: false,
                message: error.message || 'Something went wrong',
            })
        }

        next()

    }
}