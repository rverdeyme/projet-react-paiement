const { Marchand } = require('.')

module.exports = (sequelize, Sequelize) => {
    const Credentials = sequelize.define('credentials', {
        personalToken: {
            type: Sequelize.STRING,
            required: true
        },
        marchandId: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                // This is a reference to another model
                model: Marchand,

                // This is the column name of the referenced model
                key: 'id',

                // This declares when to check the foreign key constraint. PostgreSQL only.
                deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
            }
        }
    })

    return Credentials
}
