const Sequelize = require('sequelize')
const confDb = require('./configDb')

let conf = confDb.development

if (process.env.NODE_ENV === 'test') {
    conf = confDb.test
    console.log('Test database selected')
}

const sequelize = new Sequelize(conf.DB, conf.USER, conf.PASSWORD, {
    host: conf.HOST,
    dialect: conf.dialect,
    pool: {
        max: conf.pool.max,
        min: conf.pool.min,
        acquire: conf.pool.acquire,
        idle: conf.pool.idle
    }
})

const db = {}

db.Sequelize = Sequelize
db.sequelize = sequelize

const Marchand = require('./marchand')(sequelize, Sequelize)
const Credentials = require('./credentials')(sequelize, Sequelize)
const Options = require('./options')(sequelize, Sequelize)
const Contact = require('./contact')(sequelize, Sequelize)

Marchand.hasOne(Contact, {foreignKey: 'marchandId'})
Marchand.hasOne(Options, {foreignKey: 'marchandId'})
Marchand.hasOne(Credentials, {foreignKey: 'marchandId'})

Marchand.addHook('afterCreate', m => {
    return Promise.all([Credentials.create({marchandId: m.id}),
    Options.create({marchandId: m.id}),
    Contact.create({marchandId: m.id}),
    ])
})

db.Marchand = Marchand
db.Contact = Contact
db.Options = Options
db.Credentials = Credentials
  
module.exports = db
