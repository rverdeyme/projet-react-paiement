module.exports = {
    development: {
        HOST: process.env.POSTGRES_HOST || 'postgres',
        USER: process.env.POSTGRES_USER || 'pgp',
        PASSWORD: process.env.POSTGRES_PASSWORD || 'pgp-esgi',
        DB: process.env.POSTGRES_DB || 'marchand',
        dialect: 'postgres',
        pool: {
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 10000
        }
    },
    test: {
        HOST: process.env.POSTGRES_HOST || 'postgres',
        USER: process.env.POSTGRES_USER || 'pgp',
        PASSWORD: process.env.POSTGRES_PASSWORD || 'pgp-esgi',
        DB: 'test',
        dialect: 'postgres',
        pool: {
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 10000
        }
    }
}