const { Marchand } = require('.')

module.exports = (sequelize, Sequelize) => {
    const Contact = sequelize.define('contact', {
        name: {
            type: Sequelize.STRING,
        },
        address: {
            type: Sequelize.STRING
        },
        city: {
            type: Sequelize.STRING
        },
        country: {
            type: Sequelize.STRING
        },
        email: {
            type: Sequelize.STRING,
        },
        marchandId: {
            type: Sequelize.INTEGER,
            required: true,
            allowNull: false,
            validate: {
                notEmpty: true
            },
            unique: true,
            references: {
                // This is a reference to another model
                model: Marchand,

                // This is the column name of the referenced model
                key: 'id',

                // This declares when to check the foreign key constraint. PostgreSQL only.
                deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
            }
        }

    })

    return Contact
}
