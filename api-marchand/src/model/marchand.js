const bcrypt = require('bcrypt')

module.exports = (sequelize, Sequelize) => {
    const Marchand = sequelize.define('marchand', {
        // attributes
        societyName: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true,
            validate: {
                notEmpty: true
            }
        },
        kbis: {
            type: Sequelize.STRING,
        },
        validated: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },
        password: {
            type: Sequelize.STRING,
            allowNull: false,
            validate: {
                notEmpty: true
            },
            set(val) {
                const hash = bcrypt.hashSync(val, 2)
                this.setDataValue('password', hash)
            }
        }
    })

    Marchand.prototype.verifyPassword = function (enteredPassword) {
        return bcrypt.compare(enteredPassword, this.password)
    }

    return Marchand
}
