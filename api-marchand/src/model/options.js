const { Marchand } = require('.')

module.exports = (sequelize, Sequelize) => {
    const Options = sequelize.define('options', {
        confirmUrl: {
            type: Sequelize.STRING,
        },
        cancelUrl: {
            type: Sequelize.STRING,
        },
        devise: {
            type: Sequelize.STRING,
        },
        marchandId: {
            type: Sequelize.INTEGER,
            allowNull: false,
            unique: true,
            references: {
                // This is a reference to another model
                model: Marchand,

                // This is the column name of the referenced model
                key: 'id',

                // This declares when to check the foreign key constraint. PostgreSQL only.
                deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
            }
        }
    })

    return Options
}