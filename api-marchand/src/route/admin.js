const controller = require('../controller/admin')
const { verifyJWTAdmin } = require('../jwt')

module.exports = app => {

    const router = require('express').Router()

    //********Route for Marchand***********//
    //Retrieve all Marchand
    router.get('/marchands', controller.findAllMarchands)

    //Retrieve a Single marchand with id
    router.get('/marchands/:marchandId', controller.findMarchandOne)

    //Update a marchand with id
    router.put('/marchands/:marchandId', controller.updateMarchand)

    //Delete a marchand with id
    router.delete('/marchands/:marchandId', controller.deleteMarchand)

    //********Route for Contact***********//
    //Retrieve a Single marchand with id
    router.get('/contact/:marchandId', controller.findContactOne)

    //Update a marchand with id
    router.put('/contact/:marchandId', controller.updateContact)

    //Delete a marchand with id
    router.delete('/contact/:marchandId', controller.deleteContact)


    //********Route for Option***********//
    //Retrieve a Single marchand with id
    router.get('/option/:marchandId', controller.findOptionOne)

    //Update a marchand with id
    router.put('/option/:marchandId', controller.updateOption)

    //Delete a marchand with id
    router.delete('/option/:marchandId', controller.deleteOption)


    //Upload marchand kbis test
    router.post('/upload', controller.testU)

    app.use('/admin', router)
}
