const { contact } = require('../controller')
const { verifyJWT } = require('../jwt')

module.exports = app => {

    const router = require('express').Router()

    // Create a new Contact
    router.post('/', contact.create)

    // Retrieve a single Contact with id
    router.get('/', contact.findOneByMarchandId)

    // Update a Contact with id
    router.put('/', contact.update)

    // Delete a Contact with id
    router.delete('/', contact.delete)


    app.use('/contact',verifyJWT, router)
}
