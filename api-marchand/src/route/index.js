module.exports = app => {
    require('./marchand')(app)
    require('./credentials')(app)
    require('./contact')(app)
    require('./auth')(app)
    require('./options')(app)
    require('./admin')(app)
}
