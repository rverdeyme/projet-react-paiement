const controller = require('../controller/marchand')
const { verifyJWT } = require('../jwt')

module.exports = app => {

    const router = require('express').Router()


    // Retrieve a single Marchand
    router.get('/', controller.getInfos)

    // Verify a marchand
    router.get('/verify', controller.verifyMarchand)

    // Update a Marchand with id
    router.put('/', controller.update)

    // Delete a Marchand with id
    router.delete('/', controller.delete)


    app.use('/marchands', verifyJWT, router)
}
