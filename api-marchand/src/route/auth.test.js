const request = require('supertest')
const app = require('../../app')
const { sequelize } = require('../model')

beforeAll(async () => {
    await sequelize.sync({ force: true })
})

describe('Auth Endpoints', () => {
    it('should create a new marchand', (done) => {
        request(app)
            .post('/auth/signup')
            .send({
                societyName: 'test',
                kbis: 'test kbis',
                password: 'test is cool',
            })
            .expect(200)
            .end((err, res) => {
                if (err) return done(err)
                expect(res.body).toHaveProperty('token')
                done()
            })
    })
})