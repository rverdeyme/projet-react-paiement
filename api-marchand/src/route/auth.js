const controller = require('../controller/auth')
const { verifyJWT } = require('../jwt')

module.exports = app => {

    const router = require('express').Router()

    router.post('/signup', controller.signUp)

    router.get('/signin', verifyJWT, controller.jwtSignIn)

    router.post('/signin', controller.signIn)

    app.use('/auth', router)
}
