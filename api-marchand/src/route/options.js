const { options } = require('../controller')
const { verifyJWT } = require('../jwt')

module.exports = app => {

    const router = require('express').Router()

    router.post('/', options.update)

    router.get('/', options.getFromMarchand)

    router.delete('/', options.delete)

    app.use('/option', verifyJWT, router)
}
