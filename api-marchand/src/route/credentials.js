const controller = require('../controller/credentials')
const { verifyJWT } = require('../jwt')

module.exports = app => {

    const router = require('express').Router()

    // Create a new Credentials
    router.post('/', controller.generateCredentials)
    
    router.get('/', controller.getCredentials)

    app.use('/credentials', verifyJWT, router)
}
