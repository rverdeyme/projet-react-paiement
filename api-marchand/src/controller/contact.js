const { Contact } = require('../model')
const contactValidator = require('../validator/contact')

module.exports = {
    create: async (req, res) => {
        try {
            const newContact = {
                ...req.body,
                marchandId: req.marchand.id
            }

            contactValidator.verifyContact(newContact)

            const data = await Contact.create(newContact)

            res.send({success: true, contact: data})
        } catch (error) {
            res.status(500).send({
                message: error.message || 'Some error occurred while creating the contact'
            })
        }
    },

    findOneByMarchandId: async (req, res) => {
        const {marchand} = req

        try {
            const contact = await marchand.getContact()
            
            res.send(contact)
        } catch (error) {
            res.status(500).send({
                message: `Error retrieving contact details from marchand with id=${marchandId}`
            })
        }
    },

    update: async (req, res) => {
        try {
            let contact = await req.marchand.getContact()
            
            contactValidator.verifyContact(contact)
            
            contact = await contact.update(req.body)
            res.send(contact)
        } catch (error) {
            res.status(500).send({
                message: error.message || `Error updating contact of marchand with id=${req.marchand.id}`
            })
        }
    },

    delete: async (req, res) => {
        const id = req.marchand.id

        try {
            const num = await Contact.destroy({
                where: { marchandId: id }
            })
            if (num == 1) {
                res.send({
                    message: 'contact was deleted successfully!'
                })
            } else {
                res.send({
                    message: `Cannot delete contact with id=${id}. Maybe contact was not found!`
                })
            }
        } catch (error) {
            res.status(500).send({
                message: `Could not delete contact with id=${id}`
            })
        }
    }
}