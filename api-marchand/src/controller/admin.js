const { Marchand, Contact, Options } = require('../model')
const marchandValidator = require('../validator/marchand')




module.exports = {

    testU: async (req,res) => {
        try {

            console.log(req)
            res.send('bon')

        } catch (error) {
            console.log(error)
            res.status(500).send({
                message: `fail to upload file `
            })
        }
    },

    findAllMarchands: async (req, res) => {
        try {

            const marchands = await Marchand.findAll()
            res.send(marchands);

        } catch (error) {
            res.status(500).send({
                message: `Cannot retrieve all marchands`
            })
        }
    },

    findMarchandOne: async (req, res) => {
        try {

            const marchandId = req.marchandId;
            const marchand = await Marchand.findOne(marchandId)

            res.send(marchand)
        } catch (error) {
            res.status(500).send({
                message: `Error retrieving marchand with id=${req.marchand.id}`
            })
        }
    },

    updateMarchand: async (req, res) => {
        try {

            const marchandId = req.params.marchandId

            let marchand = {
                ...req.body
            }

            marchandValidator.verifyMarchand(marchand)

            const num = await Marchand.update(req.body, {
                where: { id: marchandId }
            })
            if (num == 1) {
                res.send({
                    message: 'Marchand was updated successfully.'
                })
            } else {
                res.send({
                    message: `Cannot update marchand with id=${req.marchandId}. Maybe marchand was not found or req.body is empty!`
                })
            }
        } catch (error) {
            res.status(500).send({
                message: error.message || `Error updating marchand with id=${req.marchandId}`
            })
        }
    },

    // Delete a Tutorial with the specified id in the request
    deleteMarchand: async (req, res) => {
        try {
            const num = await Marchand.destroy({
                where: { id: req.params.marchandId }
            })
            if (num == 1) {
                res.send({
                    message: 'marchand was deleted successfully!'
                })
            } else {
                res.send({
                    message: `Cannot delete marchand with id=${req.params.marchandId}. Maybe marchand was not found!`
                })
            }
        } catch (error) {
            res.status(500).send({
                message: `Could not delete marchand with id=${req.params.marchandId}`
            })
        }
    },

    findContactOne: async (req, res) => {
        try {

            const marchandId = req.marchandId;
            const contact = await Contact.findOne(marchandId)

            res.send(contact)
        } catch (error) {
            res.status(500).send({
                message: `Error retrieving marchand with id=${req.marchand.id}`
            })
        }
    },

    updateContact: async (req, res) => {
        try {

            const marchandId = req.marchandId;
            const contact = await Contact.findOne(marchandId)

            res.send(contact)
        } catch (error) {
            res.status(500).send({
                message: `Error retrieving marchand with id=${req.marchand.id}`
            })
        }
    },

    deleteContact : async (req, res) => {
        try {

            const marchandId = req.marchandId;
            const contact = await Contact.findOne(marchandId)

            res.send(contact)
        } catch (error) {
            res.status(500).send({
                message: `Error retrieving marchand with id=${req.marchand.id}`
            })
        }
    },

    findOptionOne: async (req, res) => {
        try {

            const marchandId = req.marchandId;
            const option = await Options.findOne(marchandId)

            res.send(option)
        } catch (error) {
            res.status(500).send({
                message: `Error retrieving marchand with id=${req.marchand.id}`
            })
        }
    },

    updateOption: async (req, res) => {
        try {

            const marchandId = req.marchandId;
            const option = await Options.findOne(marchandId)

            res.send(option)
        } catch (error) {
            res.status(500).send({
                message: `Error retrieving marchand with id=${req.marchand.id}`
            })
        }
    },

    deleteOption : async (req, res) => {
        try {

            const marchandId = req.marchandId;
            const option = await Options.findOne(marchandId)

            res.send(option)
        } catch (error) {
            res.status(500).send({
                message: `Error retrieving marchand with id=${req.marchand.id}`
            })
        }
    },



    verifyMarchand: async (req, res) => {
        if (req.marchand) { return res.send({ success: true }) }

        return res.send({ success: false })
    }
}
