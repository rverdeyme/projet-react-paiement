const { Marchand, Contact } = require('../model')
const { Op } = require('sequelize')
const { generateToken } = require('../jwt')
const marchandValidator = require('../validator/marchand')
const contactValidator = require('../validator/contact')
const { sendCreationMail } = require('../mailer')

module.exports = {
    signUp: async (req, res) => {
        try {
            const { marchand, contact } = req.body
            marchandValidator.verifyMarchand(marchand)
            contactValidator.verifyContactOnCreateWithMarchand(contact)

            const newMarchand = await Marchand.create(marchand)
            newMarchand.getContact()
                .then(c => c.update(contact))

            sendCreationMail(contact)

            res.send({ success: true })

        } catch (error) {
            console.error(error)
            res.status(500).send({
                message: error.message || 'Some error occurred while creating the marchand'
            })
        }
    },

    signIn: async (req, res) => {
        try {
            const { societyName, password } = req.body

            if (!societyName || !password) return res.status(400).send({
                message: 'You must provide the society name and password'
            })

            let marchand = await Marchand.findOne({ where: { societyName } })

            if (!marchand) return res.status(404).send({
                message: 'Society not found'
            })

            const passwordCheck = await marchand.verifyPassword(password)

            if (!passwordCheck) return res.status(404).send({
                message: 'Wrong password'
            })

            const contact = await marchand.getContact()

            const token = generateToken(marchand)

            res.send({ success: true, token, user: { ...marchand.get({
                plain: true
              }), ...contact.get({
                plain: true
              }) } })

        } catch (error) {
            res.status(500).send({
                message: error.message || 'Some error occurred while creating the marchand'
            })
        }
    },

    jwtSignIn: async (req, res) => {
        try {
            const { marchand } = req

            const contact = await marchand.getContact()

            res.send({ success: true, user: { ...marchand.get({
                    plain: true
                }), ...contact.get({
                    plain: true
                }) } })

        } catch (error) {
            res.status(500).send({
                message: error.message || 'Some error occurred while creating the marchand'
            })
        }
    },

    // Delete a Tutorial with the specified id in the request
    delete: async (req, res) => {
        const id = req.params.id

        try {
            const num = await Marchand.destroy({
                where: { id: id }
            })
            if (num == 1) {
                res.send({
                    message: 'marchand was deleted successfully!'
                })
            } else {
                res.send({
                    message: `Cannot delete marchand with id = ${id}.Maybe marchand was not found!`
                })
            }
        } catch (error) {
            res.status(500).send({
                message: `Could not delete marchand with id = ${id} `
            })
        }
    }
}
