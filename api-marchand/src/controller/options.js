const { Options } = require('../model')
const optionsValidator = require('../validator/options')

module.exports = {
    create: async (req, res) => {
        try {
            const options = {
                ...req.body,
                marchandId: req.marchand.id
            }

            optionsValidator.verifyOptions(options)

            const data = await Options.create(options)
            if (!data) throw new Error('Cannot add new option')


            res.send({ success: true, options: data })
        } catch (error) {
            res.status(500).send({
                message: error.message || 'Some error occurred while creating the option'
            })
        }
    },

    getFromMarchand: async (req, res) => {
        const { marchand } = req

        try {
            const options = await marchand.getOption()

            const {confirmUrl, cancelUrl, devise} = options

            res.send({ success: true, options: {confirmUrl, cancelUrl, devise} })
        } catch (error) {
            res.status(400).send({
                message: error.message || `Error retrieving options for marchand : ${marchand._id}`
            })
        }
    },

    update: async (req, res) => {
        const { marchand } = req
        // TODO Validate request
        try {

            const validation = optionsValidator.verifyOptions({ ...req.body, marchandId: marchand.id })

            console.log({validation})
            let options = await marchand.getOption()

            options = await options.update({
                ...req.body
            })

            res.send({
                success: true,
                options
            })
        } catch (error) {
            console.log(error)
            res.status(500).send({
                success: false,
                message: error.message || `Error updating option of marchand with id=${req.marchand.id}`
            })
        }
    },

    delete: async (req, res) => {
        const marchandId = req.marchand.id

        try {
            const num = await Options.destroy({
                where: { marchandId: marchandId }
            })
            if (num == 1) {
                res.send({
                    message: 'option was deleted successfully!'
                })
            } else {
                res.send({
                    message: `Cannot delete option of marchand with id=${marchandId}. Maybe option was not found!`
                })
            }
        } catch (error) {
            res.status(500).send({
                message: `Could not delete option of marchand with id=${marchandId}`
            })
        }
    },
    verifyOption: async (req, res) => {
        if (req.option) { return res.send({ success: true }) }

        return res.send({ success: false })
    }
}
