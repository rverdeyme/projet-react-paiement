const { Marchand } = require('../model')
const jwt = require('jsonwebtoken')

module.exports = {
    generateCredentials: async (req, res) => {
        try {
            const { marchandId } = req.body

            if (!marchandId) throw new Error('MarchandId not provided')

            const marchand = await Marchand.findByPk(marchandId)

            if (!marchand) throw new Error('Marchand not found with id : ' + marchandId)

            const token = await jwt.sign({ marchandId }, process.env.JWT_SECRET)

            const credentials = await marchand.getCredential()

            credentials.personalToken = token
            await credentials.save()

            res.send({
                sucess: true,
                credentials
            })
        } catch (error) {
            console.error(error)
            res.status(400).send({
                sucess: false,
                message: error.message || 'Something went wrong'
            })
        }
    },

    getCredentials: async (req, res) => {
        try {
            const { marchand } = req
            const credentials = await marchand.getCredential()

            res.send({credentials})
        } catch (error) {
            console.error(error)
            res.status(400).send({
                sucess: false,
                message: error.message || 'Something went wrong'
            })
        }
    } 
}