module.exports = {
    marchand: require('./marchand'),
    auth: require('./auth'),
    contact: require('./contact'),
    credentials: require('./credentials'),
    options: require('./options')
}
