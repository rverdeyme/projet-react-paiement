const { Marchand } = require('../model')
const marchandValidator = require('../validator/marchand')

module.exports = {

    getInfos: async (req, res) => {
        try {
            const {
                societyName,
                kbis,
                validated,
                createdAt
            } = req.marchand
            res.send({societyName,
                kbis,
                validated,
                createdAt})
        } catch (error) {
            res.status(500).send({
                message: `Error retrieving marchand with id=${req.marchand.id}`
            })
        }
    },


    findAll: async (req, res) => {
      try {

        const marchands = await Marchand.findAll()
        res.send(marchands);

      } catch (error) {
        res.status(500).send({
          message: `Cannot retrieve all marchands`
        })
      }
    },

    findAdminOne: async (req, res) => {
      try {

        const marchandId = req.marchandId;
        const marchand = await Marchand.findOne(marchandId)

        res.send(marchand)
      } catch (error) {
        res.status(500).send({
          message: `Error retrieving marchand with id=${req.marchand.id}`
        })
      }
    },

    update: async (req, res) => {
        try {

            let {marchand} = req

            // marchandValidator.verifyMarchand(marchand)

            const {
                societyName,
                kbis,
                validated,
                createdAt
            } = await marchand.update(req.body)

            res.send({
                societyName,
                kbis,
                validated,
                createdAt
            })
        } catch (error) {
            res.status(500).send({
                message: error.message || `Error updating marchand with id=${req.marchand.id}`
            })
        }
    },

    updateAdmin: async (req, res) => {
        try {

            const marchandId = req.params.marchandId

            let marchand = {
                ...req.body
            }

            marchandValidator.verifyMarchand(marchand)

            const num = await Marchand.update(req.body, {
                where: { id: marchandId }
            })
            if (num == 1) {
                res.send({
                    message: 'Marchand was updated successfully.'
                })
            } else {
                res.send({
                    message: `Cannot update marchand with id=${req.marchandId}. Maybe marchand was not found or req.body is empty!`
                })
            }
        } catch (error) {
            res.status(500).send({
                message: error.message || `Error updating marchand with id=${req.marchandId}`
            })
        }
    },

    // Delete a Tutorial with the specified id in the request
    delete: async (req, res) => {
        try {
            const num = await Marchand.destroy({
                where: { id: req.marchand.id }
            })
            if (num == 1) {
                res.send({
                    message: 'marchand was deleted successfully!'
                })
            } else {
                res.send({
                    message: `Cannot delete marchand with id=${req.marchand.id}. Maybe marchand was not found!`
                })
            }
        } catch (error) {
            res.status(500).send({
                message: `Could not delete marchand with id=${req.marchand.id}`
            })
        }
    },
    deleteAdmin: async (req, res) => {
        try {
            const num = await Marchand.destroy({
                where: { id: req.params.marchandId }
            })
            if (num == 1) {
                res.send({
                    message: 'marchand was deleted successfully!'
                })
            } else {
                res.send({
                    message: `Cannot delete marchand with id=${req.params.marchandId}. Maybe marchand was not found!`
                })
            }
        } catch (error) {
            res.status(500).send({
                message: `Could not delete marchand with id=${req.params.marchandId}`
            })
        }
    },

    verifyMarchand: async (req, res) => {
        if (req.marchand) { return res.send({ success: true }) }

        return res.send({ success: false })
    }
}
