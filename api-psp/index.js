const express = require('express')
const bodyParser = require('body-parser')
const helmet = require('helmet')
const spdy = require('spdy')

const app = express()

app.use(helmet())
// parse requests of content-type - application/json
app.use(bodyParser.json())

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

app.post('/', (req, res) => {
    try {
        const { cardDetails, amount, devise } = req.body

        if (!cardDetails || !amount || !devise) throw new Error()

        setTimeout(() => res.send({ success: true }), 10000)
    } catch (error) {
        console.log(error)
        res.status(400).send({
            success: false,
            message: error.message || 'You must provide all informations!'
        })
    }
})
const PORT = process.env.API_PSP_PORT || 3000

const server = spdy.createServer(require('./ssl'), app)

server.listen(PORT, (error) => {
    if (error) {
        console.error(error)
    } else {
        console.log(`Listening on port: ${PORT}`)
    }
})

module.exports = app
