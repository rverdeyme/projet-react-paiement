import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import Button from '@material-ui/core/Button'
import CssBaseline from '@material-ui/core/CssBaseline'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles'
import Container from '@material-ui/core/Container'

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
  },
  '@global': {
    ul: {
      margin: 0,
      padding: 0,
      listStyle: 'none',
    },
  },
  link: {
    margin: theme.spacing(1, 1.5),
  },
  heroContent: {
    padding: theme.spacing(8, 0, 6),
  },
  container: {
    width: 'max-content',
    '&>div': {
      display: 'flex',
      marginBottom: '5px',
      '&>span': {
        fontWeight: '700',
        margin: '0 auto 0 0',
      },
    },
  },
  button: {
    margin: '10px auto',
    width: '300px',
  },
}))

export default function Informations() {
  const classes = useStyles()
  const user = useSelector(({ user }) => user)

  return (

    <>
      <CssBaseline />
      {/* Hero unit */}
      <Container maxWidth="sm" component="main" className={classes.heroContent}>
        <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
          FastPay
        </Typography>
        <Typography variant="h5" align="center" color="textSecondary" component="p">
          My Informations
        </Typography>
      </Container>
      {/* End hero unit */}
      <Container maxWidth="md" component="main" className={classes.container}>
        <div>
          {' '}
          <span> Society : </span>
          {' '}
          {user.marchand.societyName}
          {' '}
        </div>
        <div>
          {' '}
          <span> Kbis : </span>
          {' '}
          {user.marchand.kbis}
          {' '}
        </div>
        <div>
          {' '}
          <span> Email : </span>
          {' '}
          {user.contact.email}
          {' '}
        </div>
        <div>
          {' '}
          <span> Contact name : </span>
          {' '}
          {user.contact.name}
          {' '}
        </div>
        <div>
          {' '}
          <span> Address : </span>
          {' '}
          {user.contact.address}
          {' '}
        </div>
        <div>
          {' '}
          <span> City : </span>
          {' '}
          {user.contact.city}
          {' '}
        </div>
        <div>
          {' '}
          <span> Country : </span>
          {' '}
          {user.contact.country}
          {' '}
        </div>
        <Button fullWidth variant="contained" color="primary" className={classes.button}>
          Update my informations
        </Button>
      </Container>

    </>
  )
}
