import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import AppBar from '@material-ui/core/AppBar'
import Button from '@material-ui/core/Button'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import Link from '@material-ui/core/Link'
import { Link as RouterLink } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles'
import { signOut } from '../app/store/actions'

const useStyles = makeStyles((theme) => ({
  '@global': {
    ul: {
      margin: 0,
      padding: 0,
      listStyle: 'none',
    },
  },
  appBar: {
    borderBottom: `1px solid ${theme.palette.divider}`,
  },
  toolbar: {
    flexWrap: 'wrap',
  },
  toolbarTitle: {
    flexGrow: 1,
  },
  link: {
    margin: theme.spacing(1, 1.5),
  },
  heroContent: {
    padding: theme.spacing(8, 0, 6),
  },
  cardHeader: {
    backgroundColor:
            theme.palette.type === 'light' ? theme.palette.grey[200] : theme.palette.grey[700],
  },
  cardPricing: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'baseline',
    marginBottom: theme.spacing(2),
  },
  footer: {
    borderTop: `1px solid ${theme.palette.divider}`,
    marginTop: theme.spacing(8),
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
    [theme.breakpoints.up('sm')]: {
      paddingTop: theme.spacing(6),
      paddingBottom: theme.spacing(6),
    },
  },
}))

export default function Headers() {
  const classes = useStyles()

  // eslint-disable-next-line no-shadow
  const user = useSelector(({ user }) => user)
  const dispatch = useDispatch()

  const connectionButton = () => {
    const disconnect = () => dispatch(signOut())
    if (user) {
      return (
        <Button color="primary" variant="outlined" className={classes.link} onClick={disconnect}>
          Disconnect
        </Button>
      )
    }

    return (
      <Button component={RouterLink} to="/signin" color="primary" variant="outlined" className={classes.link}>
        Login
      </Button>
    )
  }

  return (
    <AppBar position="static" color="default" elevation={0} className={classes.appBar}>
      <Toolbar className={classes.toolbar}>
        <Typography variant="h6" color="inherit" noWrap className={classes.toolbarTitle}>
          <Link component={RouterLink} to="/">
            {user ? user.societyName : 'Accueil'}
          </Link>
        </Typography>
        <nav>
          <Link variant="button" color="textPrimary" component={RouterLink} to="/account" className={classes.link}>
            Account
          </Link>
          <Link variant="button" color="textPrimary" component={RouterLink} to="/transactions" className={classes.link}>
            Transactions
          </Link>
        </nav>
        {connectionButton()}
      </Toolbar>
    </AppBar>
  )
}
