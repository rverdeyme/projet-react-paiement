import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import { Paper } from '@material-ui/core'
import { useSelector } from 'react-redux'
import Informations from './component/informations'
import Contact from './component/contact'
import Options from './component/options'
import Credentials from './component/credentials'
import Authentication from '../connection/authentication'

export default function SimpleTabs() {
  const [value, setValue] = React.useState(0)

  const user = useSelector(({ user }) => user)

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  if (!user) return <Authentication />

  return (
    <>
      <Paper square>
        <Tabs
          indicatorColor="primary"
          variant="fullWidth"
          textColor="primary"
          value={value}
          onChange={handleChange}
        >
          <Tab label="Informations" onClick={() => setValue(0)} />
          <Tab label="Contact" onClick={() => setValue(1)} />
          <Tab label="Options" onClick={() => setValue(2)} />
          <Tab label="Credentials" onClick={() => setValue(3)} />
        </Tabs>
      </Paper>
      {}
      { value === 0 && <Informations />}
      { value === 1 && <Contact />}
      { value === 2 && <Options />}
      { value === 3 && <Credentials />}

    </>
  )
}
