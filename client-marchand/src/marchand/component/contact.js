import React, { useState, useEffect } from 'react'
import { Paper } from '@material-ui/core'
import { fetchPers } from '../../app/helpers'
import styles from './styles.module.css'
import DataDisplayer, { AddressDisplayer } from './dataDisplayer'

export default function Contact() {
  const [contact, setContact] = useState(null)

  useEffect(() => {
    fetchContact()
      .then((data) => data && setContact(data))
  }, [])

  const updateContact = (newData) => {
    fetchContact({
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(newData),
    })
      .then((data) => data && setContact(data))
  }

  if (!contact) return null

  return (
    <Paper className={styles.infoContainer}>
      <DataDisplayer data={contact.email} updateFn={(value) => updateContact({ email: value })} name="Email" />
      <DataDisplayer data={contact.name} updateFn={(value) => updateContact({ name: value })} name="Name" />
      <AddressDisplayer
        data={{
          address: contact.address,
          city: contact.city,
          country: contact.country,
        }}
        updateFn={updateContact}
      />
    </Paper>
  )
}

const fetchContact = (opt) => fetchPers('api/marchand/contact', opt)
