import React, { useState, useEffect } from 'react'
import { Paper } from '@material-ui/core'
import { fetchPers } from '../../app/helpers'
import styles from './styles.module.css'
import DataDisplayer, { DeviseDisplayer } from './dataDisplayer'

export default function Options() {
  const [option, setOption] = useState(null)

  useEffect(() => {
    fetchOptions()
      .then((data) => data && setOption(data.options || {}))
  }, [])

  const updateOptions = (newData) => {
    fetchOptions({
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(newData),
    })
      .then((data) => data && setOption(data.options))
  }

  if (!option) return null

  return (
    <Paper className={styles.infoContainer}>
      <DataDisplayer data={option.confirmUrl} updateFn={(value) => updateOptions({ confirmUrl: value })} name="Confirm Url" />
      <DataDisplayer data={option.cancelUrl} updateFn={(value) => updateOptions({ cancelUrl: value })} name="Cancel Url" />
      <DeviseDisplayer data={option.devise} updateFn={(value) => updateOptions({ devise: value })} name="Devise" />
    </Paper>
  )
}

const fetchOptions = (opt) => fetchPers('api/marchand/option', opt)
