import React, { useState, useEffect } from 'react'
import {
  Paper, TextField, IconButton, FormControl, InputLabel, Select, NativeSelect,
} from '@material-ui/core'
import CreateIcon from '@material-ui/icons/Create'
import DoneIcon from '@material-ui/icons/Done'
import styles from './styles.module.css'

const DataDisplayer = ({ data, updateFn, name }) => {
  const [toggleModification, setToggleModification] = useState(false)
  const [localData, setLocalData] = useState(data || '')

  const update = () => {
    setToggleModification(false)
    if (localData === data) { return }
    updateFn(localData)
  }

  return (
    <Paper className={styles.data}>
      <h3>
        {name}
        {' '}
        :
        {' '}
      </h3>
      {!toggleModification
        ? <h2>{data || 'Not set'}</h2>
        : (
          <TextField
            variant="outlined"
            margin="normal"
            fullWidth
            label={name}
            value={localData}
            onChange={(e) => setLocalData(e.target.value)}
            autoFocus
          />
        )}
      {!toggleModification
        ? (
          <IconButton aria-label="update" onClick={() => setToggleModification(true)}>
            <CreateIcon />
          </IconButton>
        )
        : (
          <IconButton aria-label="update" onClick={update}>
            <DoneIcon />
          </IconButton>
        )}
    </Paper>
  )
}

export const DeviseDisplayer = ({ data, updateFn }) => {
  const [toggleModification, setToggleModification] = useState(false)
  const [localData, setLocalData] = useState(data || '')

  const update = () => {
    setToggleModification(false)
    if (localData === data) { return }
    updateFn(localData)
  }

  useEffect(() => {
    console.log({ localData })
  }, [localData])

  return (
    <Paper className={styles.data}>
      <h3>
        Devise :
      </h3>
      {!toggleModification
        ? <h2>{data || 'Not set'}</h2>
        : (
          <FormControl variant="filled">
            <NativeSelect
              value={localData}
              onChange={(e) => setLocalData(e.target.value)}
              inputProps={{
                name: 'devise',
                id: 'filled-age-native-simple',
              }}
              autoFocus
            >
              <option value="" disabled>
                Devise
              </option>
              <option value="EUR">EUR</option>
              <option value="JPY">JPY</option>
              <option value="USD">USD</option>
            </NativeSelect>
          </FormControl>
        )}
      {!toggleModification
        ? (
          <IconButton aria-label="update" onClick={() => setToggleModification(true)}>
            <CreateIcon />
          </IconButton>
        )
        : (
          <IconButton aria-label="update" onClick={update}>
            <DoneIcon />
          </IconButton>
        )}
    </Paper>
  )
}

export default DataDisplayer

export function AddressDisplayer({ data, updateFn }) {
  const [localData, setLocalData] = useState(data)
  const [toggleModification, setToggleModification] = useState(false)

  const update = () => {
    setToggleModification(false)
    if (localData === data) { return }
    updateFn(localData)
  }

  const updateField = (field) => (e) => {
    e.persist()
    setLocalData((s) => ({ ...s, [field]: e.target.value }))
  }

  return (
    <Paper className={styles.address}>
      <h3>
        Address :
      </h3>
      {!toggleModification
        ? (
          <>
            <h4>
              Street :
              {' '}
              {localData.address || 'Not set'}
            </h4>
            <h4>
              City :
              {' '}
              {localData.city || 'Not set'}
            </h4>
            <h4>
              Country :
              {' '}
              {localData.country || 'Not set'}
            </h4>
          </>
        )
        : (
          <>
            <TextField
              variant="outlined"
              margin="normal"
              fullWidth
              label="Street"
              value={localData.address}
              onChange={updateField('address')}
              autoFocus
            />
            <TextField
              variant="outlined"
              margin="normal"
              fullWidth
              label="City"
              value={localData.city}
              onChange={updateField('city')}
            />
            <TextField
              variant="outlined"
              margin="normal"
              fullWidth
              label="Country"
              value={localData.country}
              onChange={updateField('country')}
            />
          </>
        )}
      {!toggleModification
        ? (
          <IconButton aria-label="update" onClick={() => setToggleModification(true)}>
            <CreateIcon />
          </IconButton>
        )
        : (
          <IconButton aria-label="update" onClick={update}>
            <DoneIcon />
          </IconButton>
        )}
    </Paper>
  )
}
