import React, { useState, useEffect } from 'react'
import { Paper } from '@material-ui/core'
import CheckIcon from '@material-ui/icons/Check'
import ClearIcon from '@material-ui/icons/Clear'
import { green, red } from '@material-ui/core/colors'
import { fetchPers } from '../../app/helpers'
import styles from './styles.module.css'
import DataDisplayer from './dataDisplayer'

export default function Informations() {
  const [info, setInfo] = useState(null)

  useEffect(() => {
    fetchInformations()
      .then((data) => data && setInfo(data))
  }, [])

  const updateOptions = (newData) => {
    fetchInformations({
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(newData),
    })
      .then((data) => data && setInfo(data))
  }

  if (!info) return null

  return (
    <Paper className={styles.infoContainer}>
      <DataDisplayer data={info.societyName} updateFn={(value) => updateOptions({ societyName: value })} name="Society Name" />
      <DataDisplayer data={info.kbis} updateFn={(value) => updateOptions({ kbis: value })} name="Kbis" />
      <Paper className={styles.data}>
        <h3>Validated : </h3>
        <h2>
          {info.validated
            ? (<CheckIcon fontSize="large" style={{ color: green[500] }} />)
            : (<ClearIcon fontSize="large" style={{ color: red[500] }} />)}

        </h2>
      </Paper>
      <Paper className={styles.data}>
        <h3>Created : </h3>
        <h2>{new Intl.DateTimeFormat('fr-FR').format(new Date(info.createdAt))}</h2>
      </Paper>
    </Paper>
  )
}

const fetchInformations = (opt) => fetchPers('api/marchand/marchands', opt)
