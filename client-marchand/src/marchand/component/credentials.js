import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import { Paper, IconButton } from '@material-ui/core'
import CachedIcon from '@material-ui/icons/Cached'
import { fetchPers } from '../../app/helpers'
import styles from './styles.module.css'

export default function Credentials() {
  const [credentials, setCredentials] = useState(null)

  const id = useSelector(({ user }) => user.id)

  useEffect(() => {
    fetchCredentials()
      .then((data) => data && setCredentials(data.credentials || {}))
  }, [])

  const generateToken = async () => {
    const data = await fetchCredentials({
      method: 'POST',
      headers: {
        'content-type': 'application/json',
      },
      body: JSON.stringify({ marchandId: id }),
    })
    setCredentials(data.credentials || {})
  }

  if (!credentials) return null

  return (
    <Paper className={styles.infoContainer}>
      <Paper className={styles.data}>
        <h2>Secret Token : </h2>
        <h3>{credentials.personalToken || 'Not set'}</h3>
        <IconButton aria-label="update" onClick={generateToken}>
          <CachedIcon />
        </IconButton>
      </Paper>
    </Paper>
  )
}

const fetchCredentials = (opt) => fetchPers('api/marchand/credentials', opt)
