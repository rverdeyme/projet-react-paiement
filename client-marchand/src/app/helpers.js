import store from './store'
import { LOADING, DONE_LOADING, NEW_ERROR } from './store/constant'
import { newError } from './store/actions'

const baseUrl = process.env.EXTERNAL_BASE_URL || 'https://localhost/'

export const fetchPers = async (uri = '', options = {}) => {
  const url = baseUrl + uri

  const token = localStorage.getItem('jwt')

  if (token) {
    options.headers = {
      ...options.headers,
      Authorization: `Bearer ${token}`,
    }
  }

  try {
    store.dispatch({ type: LOADING })

    const res = await fetch(url, options)

    const { success, message, ...data } = await res.json()

    if (success === false) throw new Error(message)

    return data
  } catch (error) {
    console.error(error)
    store.dispatch(newError(error.message || 'Http error'))

    return null
  } finally {
    store.dispatch({ type: DONE_LOADING })
  }
}

export const sseHandler = (uri = 'events/') => {
  const token = window.localStorage.getItem('jwt')

  if (!token) return null

  return new EventSource(baseUrl + uri + token)
}
