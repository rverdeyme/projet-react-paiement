import {
  SET_USER, NEW_ERROR, NULL_USER, NEW_SUCCESS_MESSAGE, NEW_INFO_MESSAGE,
} from './constant'
import { fetchPers } from '../helpers'

/**
 * Sends success message
 * @param {*} message
 */
export const sendSuccessMsg = (message) => ({
  type: NEW_SUCCESS_MESSAGE,
  payload: message,
})

/**
 * Sends Info message
 * @param {*} message
 */
export const sendInfoMsg = (message) => ({
  type: NEW_INFO_MESSAGE,
  payload: message,
})

/**
 * Sends error message
 * @param {*} message
 */
export const newError = (message) => ({
  type: NEW_ERROR,
  payload: message,
})

/**
 * Sets user
 * @param {*} payload
 */
export const setUser = (payload) => ({
  type: SET_USER,
  payload,
})

export const signIn = (credentials) => async (dispatch) => {
  const data = await fetchPers('api/marchand/auth/signin', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(credentials),
  })

  if (!data) return

  const {
    user, token,
  } = data

  window.localStorage.setItem('jwt', token)

  dispatch(sendSuccessMsg('Sucessfully Connected'))
  dispatch(setUser(user))
}

export const signOut = () => (dispatch) => {
  window.localStorage.removeItem('jwt')

  dispatch(sendInfoMsg('Disconnected'))

  dispatch({
    type: NULL_USER,
  })
}

export const storageSignIn = () => async (dispatch) => {
  if (!window.localStorage.getItem('jwt')) return

  const data = await fetchPers('api/marchand/auth/signin')

  if (!data) {
    window.localStorage.removeItem('jwt')
    return
  }

  dispatch(setUser(data.user))
}
