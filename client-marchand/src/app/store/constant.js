export const NEW_ERROR = 'new_error'

export const NEW_SUCCESS_MESSAGE = 'success_message'
export const NEW_WARN_MESSAGE = 'warn_message'
export const NEW_INFO_MESSAGE = 'info_message'

export const MARCHAND_CREATED = 'marchand_created'

export const SET_USER = 'set_user'
export const NULL_USER = 'null_user'

export const LOADING = 'loading'
export const DONE_LOADING = 'done_loading'
