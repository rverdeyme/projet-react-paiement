import { combineReducers } from 'redux'

import * as constants from './constant'

const messageReducer = (state = [], { type, payload }) => {
  let messages
  const now = Date.now()
  switch (type) {
    case constants.NEW_SUCCESS_MESSAGE:
      messages = Array.from(state)
      messages.push({
        id: now,
        message: payload,
        type: 'success',
      })
      return messages
    case constants.NEW_ERROR:
      messages = Array.from(state)
      messages.push({
        id: now,
        message: payload,
        type: 'error',
      })
      return messages
    case constants.NEW_WARN_MESSAGE:
      messages = Array.from(state)
      messages.push({
        id: now,
        message: payload,
        type: 'warn',
      })
      return messages
    case constants.NEW_INFO_MESSAGE:
      messages = Array.from(state)
      messages.push({
        id: now,
        message: payload,
        type: 'info',
      })
      return messages

    default:
      return state
  }
}

const userReducer = (state = null, { type, payload }) => {
  switch (type) {
    case constants.SET_USER:
      return {
        ...payload,
      }
    case constants.NULL_USER:
      return null

    default:
      return state
  }
}

const loadingReducer = (state = false, { type }) => {
  switch (type) {
    case constants.LOADING:
      return true
    case constants.DONE_LOADING:
      return false
    default:
      return state
  }
}

const rootReducer = combineReducers({
  messages: messageReducer,
  user: userReducer,
  loading: loadingReducer,
})

export default rootReducer
