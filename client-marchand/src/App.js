import React, { useEffect } from 'react'
import { Route, Switch } from 'react-router-dom'
import { useDispatch } from 'react-redux'
// import styles from './App.module.css'
import MarchandForm from './connection/signUp'
import SignIn from './connection/signIn'
import Home from './home/home'
import Informations from './home/informations'
import Headers from './home/headers'
import Footer from './home/footer'
import Account from './marchand'
import Transactions from './transaction'
import { storageSignIn } from './app/store/actions'
import Utils from './app/utils'

function App() {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(storageSignIn())
  }, [])

  return (
    <>
      <Utils />
      <Headers />
      <Switch>
        <Route path="/signup">
          <MarchandForm />
        </Route>
        <Route path="/signin">
          <SignIn />
        </Route>
        <Route path="/account">
          <Account />
        </Route>
        <Route path="/transactions">
          <Transactions />
        </Route>
        <Route exact path="/">
          <Home />
        </Route>

        <Route>
          <h1>404</h1>
        </Route>
      </Switch>
      <Footer />
    </>
  )
}

export default App
