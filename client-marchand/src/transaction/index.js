import React from 'react'
import { useSelector } from 'react-redux'
import Authentication from '../connection/authentication'
import OrderList from './transaction'

export default function Transaction() {
  const user = useSelector((state) => state.user)

  if (!user) return <Authentication />
  return (
    <OrderList />
  )
}
