/* eslint-disable no-underscore-dangle */
import React, { useState, useEffect } from 'react'
import PerfectScrollbar from 'react-perfect-scrollbar'
import {
  makeStyles,
  Card,
  CardActions,
  CardHeader,
  CardContent,
  Button,
  Divider,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Tooltip,
  TableSortLabel,
} from '@material-ui/core'

import ArrowRightIcon from '@material-ui/icons/ArrowRight'
import { useDispatch, useSelector } from 'react-redux'
import { fetchPers, sseHandler } from '../app/helpers'
import { newError } from '../app/store/actions'
import Authentication from '../connection/authentication'
import Details from './details'

const useStyles = makeStyles((theme) => ({
  root: {},
  content: {
    padding: 0,
  },
  inner: {
    minWidth: 800,
  },
  statusContainer: {
    display: 'flex',
    alignItems: 'center',
  },
  status: {
    marginRight: theme.spacing(1),
  },
  actions: {
    justifyContent: 'flex-end',
  },
}))

const sse = sseHandler()

export default function OrderList() {
  const user = useSelector((state) => state.user)
  const classes = useStyles()
  const dispatch = useDispatch()
  const [orders, setOrders] = useState([])
  const [transactions, setTransactions] = useState(null)

  const updateTransactions = async () => {
    const { data } = await fetchAllTransaction()

    setTransactions(data)

    const reducedData = data.reduce((acc, val) => {
      const tr = val.transactions
        .sort((a, b) => (Date.parse(a.createdAt) < Date.parse(b.createdAt) ? 1 : -1))[0]
      return ([
        ...acc, {
          ...tr,
          id: tr.reference,
        },
      ])
    }, [])

    setOrders(reducedData)
  }

  useEffect(() => {
    updateTransactions()
    sse.onmessage = (message) => {
      try {
        const { type } = JSON.parse(message.data)

        if (type.includes('transaction')) updateTransactions()
      } catch (error) {
        dispatch(newError(error.message))
      }
    }

    return () => sse.close()
  }, [])

  if (!user) return <Authentication />

  return (
    <Card
      className={classes.root}
    >
      <CardHeader
        title="Transactions"
      />
      <Divider />
      <CardContent className={classes.content}>
        <PerfectScrollbar>
          <div className={classes.inner}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Order Ref</TableCell>
                  <TableCell>Amount</TableCell>
                  <TableCell sortDirection="desc">
                    <Tooltip
                      enterDelay={300}
                      title="Sort"
                    >
                      <TableSortLabel
                        active
                        direction="desc"
                      >
                        Date
                      </TableSortLabel>
                    </Tooltip>
                  </TableCell>
                  <TableCell>Status</TableCell>
                  <TableCell> </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {orders.map((order) => (
                  <TableRow
                    hover
                    key={order._id}
                  >
                    <TableCell>{order._id}</TableCell>
                    <TableCell>
                      {new Intl.NumberFormat('fr-FR', { style: 'currency', currency: order.devise }).format(parseFloat(order.amount))}
                    </TableCell>
                    <TableCell>
                      {new Intl.DateTimeFormat('fr-FR').format(new Date(order.createdAt))}
                    </TableCell>
                    <TableCell>
                      <div className={classes.statusContainer}>
                        {order.status}
                      </div>
                    </TableCell>
                    <TableCell>
                      <Details transactionDetails={transactions.find(({ _id }) => _id === order.id).transactions} />
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </div>
        </PerfectScrollbar>
      </CardContent>
      <Divider />
      <CardActions className={classes.actions}>
        <Button
          color="primary"
          size="small"
          variant="text"
        >
          View all
          {' '}
          <ArrowRightIcon />
        </Button>
      </CardActions>
    </Card>
  )
}

export const fetchAllTransaction = (opt) => fetchPers('api/transaction/data', opt)
