import React, { useState } from 'react'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import Slide from '@material-ui/core/Slide'

const Transition = React.forwardRef((props, ref) => <Slide direction="up" ref={ref} {...props} />)

export default function Details({ transactionDetails }) {
  const initialTransaction = transactionDetails.find(({ status }) => status === 'initialized')
  const details = initialTransaction.paymentDetails
  const { devise, amount } = initialTransaction
  const [open, setOpen] = useState(false)

  const handleClickOpen = () => {
    setOpen(true)
  }

  const handleClose = () => {
    setOpen(false)
  }

  return (
    <div>
      <Button variant="outlined" color="primary" onClick={handleClickOpen}>
        See details
      </Button>
      <Dialog
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title">Details</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
            Client:
            {' '}
            {details.name}
            {' '}
            <br />
            Email:
            {' '}
            {details.email}
            {' '}
            <br />
            Amount:
            {' '}
            {new Intl.NumberFormat('de-DE', { style: 'currency', currency: devise }).format(amount)}
            {' '}
            <br />
            Address:
            {' '}
            {details.address}
            {' '}
            <br />
            Cart items:
            {' '}
            {
            details.cartItems
              .map((item) => Object.entries(item).reduce((acc, [name, value]) => `${acc} ${name.toUpperCase()} : ${value}`), '')
                        }
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}
