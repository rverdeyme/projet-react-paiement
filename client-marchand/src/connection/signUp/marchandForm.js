import React from 'react'
import PropTypes from 'prop-types'
import TextField from '@material-ui/core/TextField'
import Grid from '@material-ui/core/Grid'
import {
  Container, Paper, Typography, Avatar,
} from '@material-ui/core'
import WorkIcon from '@material-ui/icons/Work'
import { makeStyles } from '@material-ui/core/styles'
import styles from './style.module.css'

const useStyles = makeStyles((theme) => ({
  avatar: {
    margin: 'auto',
  },
  container: {
    textAlign: 'center',
  },
  title: {
    margin: '10px',
  },
}))

MarchandForm.propTypes = {
  marchandDispatch: PropTypes.func.isRequired,
  marchandState: PropTypes.objectOf(PropTypes.string).isRequired,
}

export default function MarchandForm({ marchandDispatch, marchandState }) {
  const classes = useStyles()

  return (
    <Paper className={styles.formsContainer}>
      <Container component="main" maxWidth="xs" className={classes.container}>
        <Avatar className={classes.avatar}>
          <WorkIcon />
        </Avatar>
        <Typography component="h1" variant="h5" className={classes.title}>
          Society
        </Typography>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <TextField
              variant="outlined"
              required
              fullWidth
              value={marchandState.societyName}
              onChange={(e) => marchandDispatch({ type: 'societyName', payload: e.target.value })}
              id="societyName"
              label="Society Name"
              name="societyName"
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              variant="outlined"
              fullWidth
              value={marchandState.kbis}
              onChange={(e) => marchandDispatch({ type: 'kbis', payload: e.target.value })}
              id="kbis"
              label="Kbis"
              name="kbis"
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              variant="outlined"
              required
              fullWidth
              value={marchandState.password}
              onChange={(e) => marchandDispatch({ type: 'password', payload: e.target.value })}
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
            />
          </Grid>
        </Grid>
      </Container>
    </Paper>
  )
}
