import React, { useReducer } from 'react'
import Button from '@material-ui/core/Button'
import { useDispatch } from 'react-redux'

import Avatar from '@material-ui/core/Avatar'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles'
import { Link as RouterLink, useHistory } from 'react-router-dom'
import { Link } from '@material-ui/core'

import { fetchPers } from '../../app/helpers'

import MarchandForm from './marchandForm'
import ContactForm from './contactForm'
import { NEW_ERROR, NEW_SUCCESS_MESSAGE } from '../../app/store/constant'

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  formContainer: {
    display: 'flex',
    flexWrap: 'wrap',
    '&>div:nth-child(1)': {
      margin: '0 10px 0 auto',
    },
    '&>div:nth-child(2)': {
      margin: '0 auto 0 10px',
    },
    '&>div:nth-child(3)': {
      width: '100%',
    },
  },
  signUpBtn: {
    display: 'flex',
    flexDirection: 'column',
    textAlign: 'center',
    marginTop: '20px',
    '&>button': {
      width: '150px',
      margin: '0 auto 10px',
    },
  },
}))

export default function SignUpForm() {
  const [marchandState, marchandDispatch] = useReducer(marchandReducer, initialMarchandState)
  const [contactState, contactDispatch] = useReducer(contactReducer, initialContactState)
  const dispatch = useDispatch()
  const history = useHistory()

  const signUp = async (e) => {
    e.preventDefault()
    try {
      await fetchPers('api/marchand/auth/signup', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ marchand: marchandState, contact: contactState }),
      })

      dispatch({
        type: NEW_SUCCESS_MESSAGE,
        payload: `Successfully created ${marchandState.societyName} marchand account`,
      })

      history.push('/signin')
    } catch (error) {
      dispatch({
        type: NEW_ERROR,
        payload: error.message,
      })
    }
  }

  const classes = useStyles()

  return (
    <div className={classes.paper}>
      <Avatar className={classes.avatar}>
        <LockOutlinedIcon />
      </Avatar>
      <Typography component="h1" variant="h5">
        Sign up
      </Typography>
      <form className={classes.form} onSubmit={signUp}>
        <div className={classes.formContainer}>
          <MarchandForm
            marchandDispatch={marchandDispatch}
            marchandState={marchandState}
            classes
          />
          <ContactForm
            contactDispatch={contactDispatch}
            contactState={contactState}
            classes
          />
          <div className={classes.signUpBtn}>
            <Button
              type="submit"
              variant="contained"
              color="primary"
            >
              Sign Up
            </Button>
            <Link component={RouterLink} to="/signin" variant="body2">
              Already have an account? Sign in
            </Link>
          </div>
        </div>
      </form>
    </div>
  )
}

const initialMarchandState = {
  societyName: '',
  kbis: '',
  password: '',
}

function marchandReducer(state, { type, payload }) {
  switch (type) {
    case 'societyName':
      return {
        ...state,
        societyName: payload,
      }
    case 'kbis':
      return {
        ...state,
        kbis: payload,
      }
    case 'password':
      return {
        ...state,
        password: payload,
      }

    default:
      return state
  }
}

const initialContactState = {
  name: '',
  address: '',
  city: '',
  country: '',
  email: '',
}

function contactReducer(state, { type, payload }) {
  switch (type) {
    case 'name':
      return {
        ...state,
        name: payload,
      }
    case 'address':
      return {
        ...state,
        address: payload,
      }
    case 'city':
      return {
        ...state,
        city: payload,
      }
    case 'country':
      return {
        ...state,
        country: payload,
      }
    case 'email':
      return {
        ...state,
        email: payload,
      }

    default:
      return state
  }
}
