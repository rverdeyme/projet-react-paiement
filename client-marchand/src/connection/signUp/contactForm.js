import React from 'react'
import PropTypes from 'prop-types'
import TextField from '@material-ui/core/TextField'
import Grid from '@material-ui/core/Grid'
import {
  Container, Paper, Typography, Avatar,
} from '@material-ui/core'
import ContactMailIcon from '@material-ui/icons/ContactMail'
import { makeStyles } from '@material-ui/core/styles'
import styles from './style.module.css'

const useStyles = makeStyles((theme) => ({
  avatar: {
    margin: 'auto',
  },
  container: {
    textAlign: 'center',
  },
  title: {
    margin: '10px',
  },
}))

ContactForm.propTypes = {
  contactDispatch: PropTypes.func.isRequired,
  contactState: PropTypes.objectOf(PropTypes.string).isRequired,
}

export default function ContactForm({ contactDispatch, contactState }) {
  const classes = useStyles()

  return (
    <Paper className={styles.formsContainer}>
      <Container component="main" maxWidth="xs" className={classes.container}>
        <Avatar className={classes.avatar}>
          <ContactMailIcon />
        </Avatar>
        <Typography component="h1" variant="h5" className={classes.title}>
          Contact Informations
        </Typography>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <TextField
              variant="outlined"
              type="email"
              required
              fullWidth
              value={contactState.email}
              onChange={(e) => contactDispatch({ type: 'email', payload: e.target.value })}
              id="email"
              label="Email"
              name="email"
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              variant="outlined"
              fullWidth
              value={contactState.name}
              onChange={(e) => contactDispatch({ type: 'name', payload: e.target.value })}
              id="name"
              label="Contact name"
              name="name"
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              variant="outlined"
              fullWidth
              value={contactState.address}
              onChange={(e) => contactDispatch({ type: 'address', payload: e.target.value })}
              id="address"
              label="Address"
              name="address"
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              variant="outlined"
              fullWidth
              value={contactState.city}
              onChange={(e) => contactDispatch({ type: 'city', payload: e.target.value })}
              id="city"
              label="City"
              name="city"
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              variant="outlined"
              fullWidth
              value={contactState.country}
              onChange={(e) => contactDispatch({ type: 'country', payload: e.target.value })}
              id="country"
              label="Country"
              name="country"
            />
          </Grid>
        </Grid>
      </Container>
    </Paper>
  )
}
