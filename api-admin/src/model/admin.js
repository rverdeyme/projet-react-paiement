const bcrypt = require('bcrypt')

module.exports = (sequelize, Sequelize) => {
    const Admin = sequelize.define('admin', {
        // attributes
        login: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true,
            validate: {
                notEmpty: true
            }
        },
        password: {
            type: Sequelize.STRING,
            allowNull: false,
            validate: {
                notEmpty: true
            },
            set(val) {
                const hash = bcrypt.hashSync(val, 2)
                this.setDataValue('password', hash)
            }
        }
    })

    Admin.prototype.verifyPassword = function (enteredPassword) {
        return bcrypt.compare(enteredPassword, this.password)
    }

    return Admin
}
