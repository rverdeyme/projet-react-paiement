module.exports = {
    development: {
        HOST: process.env.ADMIN_POSTGRES_HOST || 'admin_postgres',
        USER: process.env.ADMIN_POSTGRES_USER || 'pgp',
        PASSWORD: process.env.ADMIN_POSTGRES_PASSWORD || 'pgp-esgi',
        DB: process.env.ADMIN_POSTGRES_DB || 'admin',
        dialect: 'postgres',
        pool: {
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 10000
        }
    },
    test: {
        HOST: process.env.ADMIN_POSTGRES_HOST || 'admin_postgres',
        USER: process.env.ADMIN_POSTGRES_USER || 'pgp',
        PASSWORD: process.env.ADMIN_POSTGRES_PASSWORD || 'pgp-esgi',
        DB: 'test',
        dialect: 'postgres',
        pool: {
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 10000
        }
    }
}