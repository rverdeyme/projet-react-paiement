const jwt = require('jsonwebtoken')
const { Admin } = require('../model')

module.exports = {
    generateToken: function (admin) {
        //1. Dont use password and other sensitive fields
        //2. Use fields that are useful in other parts of the
        //app/collections/models
        return jwt.sign({ admin: true }, process.env.JWT_SECRET_ADMIN || 'pgp-esgi-admin')
    }

    // verifyJWT: async (req, res, next) => {
    //     let token = req.headers['authorization']
    //
    //     if (!token) { res.status(403).send({ message: 'No token provided' }) }
    //
    //     token = token.replace('Bearer ', '')
    //
    //     try {
    //         const { adminId } = jwt.verify(token, process.env.JWT_SECRET_ADMIN || 'pgp-esgi-admin')
    //         if (!adminId) throw new Error('Not a valid token')
    //
    //
    //
    //         req.admin = await Admin.findByPk(adminId)
    //         if (!req.admin) throw new Error('Cannot find admin')
    //
    //     } catch (error) {
    //         return res.status(404).send({
    //             success: false,
    //             message: error.message || 'Something went wrong',
    //         })
    //     }
    //
    //     next()
    // }
}