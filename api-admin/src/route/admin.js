const controller = require('../controller/admin')
const { verifyJWT } = require('../jwt')

module.exports = app => {

    const router = require('express').Router()

    // Retrieve a single Admin
    router.get('/', controller.findOne)


    // Update a Marchand with id
    // router.put('/', controller.update)

    // Delete a Marchand with id
    router.delete('/', controller.delete)

    app.use('/admin', router)
}
