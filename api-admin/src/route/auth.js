const controller = require('../controller/auth')

module.exports = app => {

    const router = require('express').Router()

    router.post('/signup', controller.signUpAdmin)

    router.post('/signin', controller.signIn)

    app.use('/auth', router)
}
