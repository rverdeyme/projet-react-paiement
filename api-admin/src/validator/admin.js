const { Validator } = require('jsonschema')

module.exports = {
    verifyAdmin: (admin) => {

        let validator = new Validator()
        let adminSchema = {
            'type': 'object',
            'properties': {
                'login': {
                    'type' : 'string',
                    'minLength': 1,
                    'errorMessage': 'Login is missing or uncorrect.'
                },
                'password': {
                    'type': 'string',
                    'minLength': 1,
                    'errorMessage': 'Password id is missing or uncorrect.'
                }
            },
            'required': ['login', 'password']
        }

        let result = validator.validate(admin, adminSchema)

        // if errors
        if (Array.isArray(result.errors) && result.errors.length) {
            let failedInputs = ''

            result.errors.forEach( (error) => {
                failedInputs += error.schema.errorMessage || error.message + ' '
            })

            throw new Error(failedInputs)
        }
    }

}