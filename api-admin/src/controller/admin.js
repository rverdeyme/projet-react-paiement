const { Admin } = require('../model')
const marchandValidator = require('../validator/admin')

module.exports = {

    findOne: async (req, res) => {
        try {
            res.send(req.admin)
        } catch (error) {
            res.status(500).send({
                message: `Error retrieving admin with id=${req.admin.id}`
            })
        }
    },

    // update: async (req, res) => {
    //     try {
    //
    //         let marchand = {
    //             ...req.body
    //         }
    //
    //         marchandValidator.verifyMarchand(marchand)
    //
    //         const num = await Marchand.update(req.body, {
    //             where: { id: req.marchand.id }
    //         })
    //         if (num == 1) {
    //             res.send({
    //                 message: 'Marchand was updated successfully.'
    //             })
    //         } else {
    //             res.send({
    //                 message: `Cannot update marchand with id=${req.marchand.id}. Maybe marchand was not found or req.body is empty!`
    //             })
    //         }
    //     } catch (error) {
    //         res.status(500).send({
    //             message: error.message || `Error updating marchand with id=${req.marchand.id}`
    //         })
    //     }
    // },

    // Delete a Tutorial with the specified id in the request
    delete: async (req, res) => {
        try {
            const num = await Marchand.destroy({
                where: { id: req.marchand.id }
            })
            if (num == 1) {
                res.send({
                    message: 'marchand was deleted successfully!'
                })
            } else {
                res.send({
                    message: `Cannot delete marchand with id=${req.marchand.id}. Maybe marchand was not found!`
                })
            }
        } catch (error) {
            res.status(500).send({
                message: `Could not delete marchand with id=${req.marchand.id}`
            })
        }
    },
    // verifyMarchand: async (req, res) => {
    //     if (req.marchand) { return res.send({ success: true }) }
    //
    //     return res.send({ success: false })
    // }
}
