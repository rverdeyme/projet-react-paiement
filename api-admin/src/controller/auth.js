const { Admin } = require('../model')
const { Op } = require('sequelize')
const { generateToken } = require('../jwt')
const adminValidator = require('../validator/admin')

module.exports = {
    signUpAdmin: async (req, res) => {
        try {
            console.log(req.body);
            const admin  = req.body

            adminValidator.verifyAdmin(admin)

            const newAdmin = await Admin.create(admin)

            res.send({
                success: true
            })

        } catch (error) {
            res.status(500).send({
                message: error.message || 'Some error occurred while creating the admin'
            })
        }
    },

    signIn: async (req, res) => {
        try {
            const { login, password } = req.body

            if (!login || !password) return res.status(400).send({
                message: 'You must provide login and password'
            })


            const admin = await Admin.findOne({ where: { login } })

            if (!admin) return res.status(404).send({
                message: 'login not found'
            })

            const passwordCheck = await admin.verifyPassword(password)

            if (!passwordCheck) return res.status(404).send({
                message: 'Wrong password'
            })

            const token = generateToken(admin)

            res.send({ success: true, token })

        } catch (error) {
            res.status(500).send({
                message: error.message || 'Some error occurred while creating the admin'
            })
        }
    },

}
